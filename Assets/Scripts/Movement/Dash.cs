using System.Collections;
using Entities;
using UnityEngine;

namespace Movement
{
    public class Dash : MonoBehaviour
    {
        public float Duration;
        public float Speed;
        public float LookAheadFactor;

        Transform targetTransform;
        Vector3 dashDirection;
        private CharacterView view;
        private ObstacleDetection detector;

        public void Initialize(Transform targetTransform, CharacterView view, ObstacleDetection detector)
        {
            this.targetTransform = targetTransform;
            this.view = view;
            this.detector = detector;
            enabled = false;
        }

        public void Do(Vector3 direction)
        {
            if (direction.magnitude > 0 && !enabled)
            {
                dashDirection = direction;
                StartCoroutine(WaitAndStop());
                view.AnimateRoll(1/Duration);
                enabled = true;
            }
        }

        void Update()
        {
            if(detector.CanMoveTowards(dashDirection * Speed, LookAheadFactor))
                targetTransform.Translate(Vector3.forward * Speed);
        }

        IEnumerator WaitAndStop()
        {
            yield return new WaitForSeconds(Duration);
            enabled = false;   
        }
    }
}