﻿using Entities;
using UnityEngine;

namespace Common
{
    public class PlayerInput : MonoBehaviour
    {
        public Character Character;

        void Update()
        {
            var movementVector = GetMovementVector();

            Character.Movement.MoveFromVector(movementVector);

            if (Input.GetButtonDown("Dash"))
                Character.Movement.Dash();
        }
        
        Vector3 GetMovementVector() => new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"));
    }
}
